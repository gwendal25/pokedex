const poke_container = document.getElementById('pokemon-table');
const image_front = document.getElementById('details-front');
const image_back = document.getElementById('details-back');
const sidenav_name = document.getElementById('details-name');
const sidenav_types = document.getElementById('details-types');
const sidenav_abilities = document.getElementById('details-abilities');
const sidenav_stats = document.getElementById('details-stats');

const gen_ids = [0, 151, 251, 386, 493, 649, 721, 809, 898];
var lockLoadGen = false;

const colors = {
    normal: '#d791a7',
    fire: '#ff0059',
    water: '#859cfd',
    grass: '#00db4d',
    electric: '#f4ff6b',
    ice: '#d5edff',
    fighting: '#ff4635',
    poison: '#b041da',
    ground: '#76481d',
    flying: '#89b2c7',
    psychic: '#ff0091',
    bug: '#005126',
    rock: '#98331f',
    ghost: '#9d5b91',
    dark: '#5d547a',
    dragon: '#00cfd9',
    steel: '#00c993',
    fairy: '#ff0069',
}
const main_types = Object.keys(colors);





async function loadPokemons(id) {
    if (!lockLoadGen) {
        await clearContainer();
        makeTableTop();
        lockLoadGen = true;

        await fetchPokemonGen(id);
        lockLoadGen = false;
    }
}

async function clearContainer() {
    if (!lockLoadGen) {
        poke_container.innerHTML = "";
    }
}

const fetchPokemonGen = async (id) => {
    const begin = gen_ids[id] + 1;
    const end = gen_ids[id + 1];
    for (let i = begin; i <= end; i++) {
        await (getPokemon(i));
    }
}

function makeTableTop() {
    const topEl = document.createElement('tr');
    topEl.classList.add('pokemon-table-header');
    topEl.innerHTML = `<th>Sprite</th>
    <th>Name</th>
    <th>Types</th>
    <th></th>`;
    poke_container.appendChild(topEl);
}

const getPokemon = async (id) => {
    const url = `https://pokeapi.co/api/v2/pokemon/${id}`;
    const res = await fetch(url);
    const data = await res.json();
    makePokemonCard(data);
}

function makePokemonCard(data) {
    // create the element
    const pokemonEl = document.createElement('tr');

    // extract data from the server's answer
    const id = data.id.toString().padStart(3, '0');
    const image = data.sprites.front_default;

    //build the element
    var pokemonInnerHTML = `<td><img src="${image}" alt=""></td>
    <td><p class="pokemon-name">#${id} ${data.name}</p></td><td>`;

    for (var i = 0; i < data.types.length; i++) {
        const type = getTypeFromData(data, i);
        const color = colors[type];
        pokemonInnerHTML += `<p class="pokemon-type" style="background-color: ${color}">${type}</p>`;
    }

    pokemonInnerHTML += `</td><td>
    <button class="pokemon-btn" onclick="getPokemonDetails(${data.id})">Show details</button>
    </td>`;

    pokemonEl.innerHTML = pokemonInnerHTML;
    poke_container.appendChild(pokemonEl);
}

function getTypeFromData(data, id) {
    var poke_types = data.types.map(type => type.type.name);
    var type = main_types.find(type => poke_types.indexOf(type) == id);
    return type;
}




const getPokemonDetails = async (id) => {
    const url = `https://pokeapi.co/api/v2/pokemon/${id}`;
    const res = await fetch(url);
    const data = await res.json();
    showPokemonDetails(data);
}

function showPokemonDetails(data) {
    const image = data.sprites.front_default;
    image_front.src = image;
    const imageBack = data.sprites.back_default;
    image_back.src = imageBack;
    sidenav_name.innerHTML = data.name;

    //Types
    sidenav_types.innerHTML = ``;

    for (let i = 0; i < data.types.length; i++) {
        var type = getTypeFromData(data, i);
        var color = colors[type];

        var divEl = document.createElement('div');
        var typeEl = `<p class="details-type" style="background-color: ${color}">${type}</p>`;
        divEl.innerHTML = typeEl;

        sidenav_types.appendChild(divEl);
    }

    //Abilities
    sidenav_abilities.innerHTML = ``;

    for (let i = 0; i < data.abilities.length; i++) {
        var name = data.abilities[i].ability.name;

        var divEl = document.createElement('div');
        var abilityEl = `<p class="details-ability">${name}</p>`;
        divEl.innerHTML = abilityEl;

        sidenav_abilities.appendChild(divEl);
    }

    //Stats
    sidenav_stats.innerHTML = ``;

    for (let i = 0; i < data.stats.length; i++) {
        var statEl = document.createElement('tr');
        statEl.classList.add('details-stat');

        var name = data.stats[i].stat.name;
        name = name.toUpperCase();
        var stat = data.stats[i].base_stat;

        statEl.innerHTML = `<td>${name}</td><td>${stat}</td>`;
        sidenav_stats.appendChild(statEl);
    }
}

loadPokemons(0);
getPokemonDetails(1);