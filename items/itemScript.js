const item_container = document.getElementById('item-container');
const item_name = document.getElementById('item-name');
const item_img = document.getElementById('item-img');
const item_effect = document.getElementById('item-effect');
const item_short_effect = document.getElementById('item-short-effect');

var lockLoad = false;




async function fetchItems(id) {

    if (!lockLoad) {
        lockLoad = true;

        await resetContainer();

        const url = `https://pokeapi.co/api/v2/item-pocket/${id}`;
        const res = await fetch(url);
        const data = await res.json();
        for (var i = 0; i < data.categories.length; i++) {
            await makeCategoryHeader(data.categories[i].name);
            await makeItems(data.categories[i]);
        }

        lockLoad = false;
    }
}

async function resetContainer() {
    item_container.innerHTML = `<tr class="item-table-header"><th>Sprite</th><th>Name</th><th>Details</th></tr>`;
}

async function makeCategoryHeader(name) {
    const itemEl = document.createElement('tr');
    itemEl.classList.add('item-category');
    itemEl.innerHTML = `<th></th><th>${name}</th><th></th>`;
    item_container.appendChild(itemEl);
}

async function makeItems(category) {
    const res = await fetch(category.url);
    const data = await res.json();
    const items = data.items;

    for (var i = 0; i < items.length; i++) {
        const res = await fetch(items[i].url);
        const data = await res.json();
        makeItemRow(data);
    }
}

function makeItemRow(data) {
    const itemEl = document.createElement('tr');
    itemEl.classList.add('item-row');

    var id = data.id;
    var image = data.sprites.default;
    var name = data.name;

    var itemInnerHTML = `
    <td><img class="item-img" src="${image}" alt=""></td>
    <td><p class="item-name">#${id} ${name}</p></td>
    <td><button class="item-btn" onclick="getItemDetails(${id})">Show details</button></td>
    `;

    itemEl.innerHTML = itemInnerHTML;
    item_container.appendChild(itemEl);
}

async function getItemDetails(id) {
    const url = `https://pokeapi.co/api/v2/item/${id}/`;
    const res = await fetch(url);
    const data = await res.json();

    item_name.innerHTML = data.name;
    item_img.src = data.sprites.default;
    item_effect.innerHTML = data.effect_entries[0].effect;
    item_short_effect.innerHTML = data.effect_entries[0].short_effect;
}

async function getAllItems() {
    await fetchItems(1);
    // await fetchItems(2);
    // await fetchItems(3);
    // await fetchItems(4);
}

getAllItems();