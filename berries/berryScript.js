const berry_container = document.getElementById('berry-container');
const berry_count = 64;
const colors = {
    spicy: '#F5AC78',
    dry: '#9DB7F5',
    sweet: '#FA92B2',
    bitter: '#A7DB8D',
    sour: '#FAE078'
};
const main_flavors = Object.keys(colors);

var lockLoadGen = false;


const fetchBerries = async () => {
    if (!lockLoadGen) {
        lockLoadGen = true;

        await resetTable();
        for (let i = 1; i <= berry_count; i++) {
            await getBerry(i);
        }

        lockLoadGen = false;
    }
}


async function resetTable() {
    berry_container.innerHTML = `
    <tr>
        <th>Sprite</th>
        <th>Name</th>
        <th>Growth time</th>
        <th>Max harvest</th>
        <th>Flavor</th>
    </tr>`;
}


const getBerry = async (id) => {
    const url = `https://pokeapi.co/api/v2/berry/${id}/`;
    const res = await fetch(url);
    const data = await res.json();
    makeBerryCard(data);
}

function makeBerryCard(data) {

    const berryEl = document.createElement('tr');
    berryEl.classList.add('berry');

    // const flavor_types = data.flavors.map(flavor => flavor.flavor.name);
    // const flavor = main_flavors.find(flavor => flavor_types.indexOf(flavor) > -1);
    // const color = colors[flavor];

    var main_flavor_potency = 0;
    var flavor_name = 'spicy';
    var flavors = data.flavors

    flavors.forEach(flavor => {
        if (flavor.potency > main_flavor_potency) {
            main_flavor_potency = flavor.potency;
            flavor_name = flavor.flavor.name;
        }
    })

    var color = '#fffff';
    switch (flavor_name) {
        case 'spicy':
            color = '#F5AC78';
            break;
        case 'dry':
            color = '#9DB7F5';
            break;
        case 'sweet':
            color = '#FA92B2';
            break;
        case 'bitter':
            color = '#A7DB8D';
            break;
        case 'sour':
            color = '#FAE078';
            break;
    }

    var berryInnerHTML = `
        <td><img class="berry-img" src="" alt=""></td>
        <td><span class="number">#${data.id}</span> ${data.name}</td>
        <td>${data.growth_time}</td>
        <td>${data.max_harvest}</td>
        <td>${flavor_name}</td>`;

    berryEl.style.backgroundColor = color;
    berryEl.innerHTML = berryInnerHTML;

    berry_container.appendChild(berryEl);
}


fetchBerries();